$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval:1500
    });
    $("#contacto").on('show.bs.modal', function(e){
        console.log('el modal funciono');

        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled',true);
    })
    $("#contacto").on('hidden.bs.modal',function(e){
        console.log('El modal e oculto');
        $('#contactoBtn').prop('disabled', false)
    })

});